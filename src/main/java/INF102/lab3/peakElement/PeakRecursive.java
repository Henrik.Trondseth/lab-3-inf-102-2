package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if(numbers.size() == 1){
            return numbers.get(0);
        }
        return numbers.get(findNumber(numbers, 0, numbers.size() - 1));
    }

    public int findNumber(List<Integer> numbers, int start, int end){
        if(start == end){
            return end;
        }
        
        int findMiddle = (start + end) / 2;

        if(numbers.get(findMiddle) > numbers.get(findMiddle + 1)){
            return findNumber(numbers, start, findMiddle);
        }
        return findNumber(numbers, findMiddle + 1, end);
    }
}
