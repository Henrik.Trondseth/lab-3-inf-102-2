package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return findSum(list, 0);
    }

    private long findSum(List<Long> list, int index) {
        if (index < list.size()) {
            Long number = list.get(index);
            long rest = findSum(list, index + 1);
            return number + rest;
        } else {
            return 0;
        }
    }
}
