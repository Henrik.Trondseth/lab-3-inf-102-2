package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int start = number.getLowerbound();
        int end = number.getUpperbound();
        int middle = (start + end) / 2;

        for(int i = 0; i <= end; i++){
            int makeGuess = number.guess(middle);
            if(makeGuess == 0){
                return middle;
            }
            if(makeGuess == -1){
                start = middle + 1; 
            }
            else{
                end = middle - 1; 
            }
            middle = (start + end) / 2;
        }

        return 0;
    }

}
